package dwla.nwt.react;

import android.app.Activity;
import android.app.Application;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import dwla.nwt.react.R;

public class einstellungen extends Fragment {


    public einstellungen() {
        // Required empty public constructor
    }
    Button btn_male,btn_female,btn_help;
    ScrollView settings_backgrd;
    TextView settings_person, settings_versuch,
            settings_label_name, settings_label_timemin, settings_label_timemax, settings_label_geschlecht;
    EditText settings_name,settings_t_min,settings_t_max;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_einstellungen,container,false);

        btn_male = (Button)v.findViewById(R.id.settings_btn_male);
        btn_female = (Button)v.findViewById(R.id.settings_btn_female);
        btn_help = (Button)v.findViewById(R.id.settings_help);
        settings_person = (TextView)v.findViewById(R.id.settings_header_person);
        settings_versuch = (TextView)v.findViewById(R.id.settings_header_versuch);
        settings_name = (EditText)v.findViewById(R.id.settings_input_name);
        settings_t_min = (EditText)v.findViewById(R.id.settings_input_t_min);
        settings_t_max = (EditText)v.findViewById(R.id.settings_input_t_max);
        settings_backgrd = (ScrollView)v.findViewById(R.id.settings_backgrd);
        creatOnClickListeners();
        if(!((App)getActivity().getApplication()).getName().equals("")){
            settings_name.setText(((App)getActivity().getApplication()).getName());
        }
        if(!(((App)getActivity().getApplication()).getTimeMin() == -1)){
            settings_t_min.setText(((App)getActivity().getApplication()).getTimeMin()+"");
        }
        if(!(((App)getActivity().getApplication()).getTimeMax() == -1)){
            settings_t_max.setText(((App)getActivity().getApplication()).getTimeMax()+"");
        }
        boolean test = ((App)getActivity().getApplication()).isGender_isMale();
        setUI(((App) getActivity().getApplication()).isGender_isMale());
        return v;

    }

    public void creatOnClickListeners(){
        btn_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(!((App)getActivity().getApplication()).isGender_isMale()){
                        ((App) getActivity().getApplication()).setGender_isMale(true);
                        setUI(true);
                        updateVersuchFragment();
                }
            }
        });
        btn_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(((App)getActivity().getApplication()).isGender_isMale()){
                    ((App) getActivity().getApplication()).setGender_isMale(false);
                    setUI(false);
                    updateVersuchFragment();
                }
            }
        });
        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog.Builder speichern_saveDialog = new MaterialDialog.Builder(getContext());
                speichern_saveDialog.content("Die beiden Zahlenwerte beschreiben wie lange es dauern kann bis man bei dem Versuch reagieren muss. Dabei wird eine Zufallszahl zwischen dem ersten und zweiten Wert gewählt. \n Der erste Wert muss kleiner als der zweite sein. \n Beide Werte müssen positiv sein.").show();
            }
        });
        settings_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String d = settings_name.getText().toString();
                ((App)getActivity().getApplication()).setName(d);
                updateVersuchFragment();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        settings_t_min.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                int i;
                if(!settings_t_min.getText().toString().equals(""))
                    if(Long.parseLong(settings_t_min.getText().toString()) < 0){
                        i = -1; //null
                        ((App)getActivity().getApplication()).setTimeMin(i);
                    }
                    else if(Integer.MAX_VALUE < Long.parseLong(settings_t_min.getText().toString())  ){
                        ((App)getActivity().getApplication()).setTimeMax(Integer.MAX_VALUE);
                        settings_t_min.setText(Integer.MAX_VALUE+"");
                        Toast.makeText(getContext().getApplicationContext(),"Der größtmögliche Wert ist "+Integer.MAX_VALUE, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        i = Integer.parseInt(settings_t_min.getText().toString());
                        ((App)getActivity().getApplication()).setTimeMin(i);
                    }
                    updateVersuchFragment();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        settings_t_max.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int i;
                if(!settings_t_max.getText().toString().equals(""))
                    if(Long.parseLong(settings_t_max.getText().toString()) < 0){
                        i = -1; //null
                        ((App)getActivity().getApplication()).setTimeMax(i);
                    }
                    else if(Integer.MAX_VALUE < Long.parseLong(settings_t_max.getText().toString())  ){
                        ((App)getActivity().getApplication()).setTimeMax(Integer.MAX_VALUE);
                        settings_t_max.setText(Integer.MAX_VALUE+"");
                        Toast.makeText(getContext().getApplicationContext(),"Der größtmögliche Wert ist "+Integer.MAX_VALUE, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        i = Integer.parseInt(settings_t_max.getText().toString());
                        ((App)getActivity().getApplication()).setTimeMax(i);
                    }
                    updateVersuchFragment();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        settings_backgrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
    void setUI(boolean i){
        int primary,secondary,tertiary,alt;
        if(i){
            primary = getResources().getColor(R.color.palette_blue_mid);
            secondary = getResources().getColor(R.color.palette_blue_dark);
            tertiary = getResources().getColor(R.color.palette_blue_light);
            alt = getResources().getColor(R.color.palette_blue_alt);
            btn_male.setTextColor(getResources().getColor(R.color.palette_blue_mid));
            btn_female.setTextColor(getResources().getColor(R.color.palette_greyscale_grey));


        }
        else{
            primary = getResources().getColor(R.color.palette_orange_mid);
            secondary = getResources().getColor(R.color.palette_orange_dark);
            tertiary = getResources().getColor(R.color.palette_orange_light);
            alt = getResources().getColor(R.color.palette_orange_alt);
            btn_female.setTextColor(getResources().getColor(R.color.palette_orange_mid));
            btn_male.setTextColor(getResources().getColor(R.color.palette_greyscale_grey));


        }

        // SET SETTINGS
        settings_versuch.setTextColor(primary);
        settings_person.setTextColor(primary);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getActivity().getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.setStatusBarColor(primary);

        }
        Activity parent = getActivity();
        if(parent instanceof Main){
            ((Main)parent).changeTabColor(primary,alt);
        }




    }
    void updateVersuchFragment(){
       if(getActivity() instanceof Main) {
           ((Main) getActivity()).refreshVersuchFragment();
       }
    }



}
