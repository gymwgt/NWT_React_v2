package dwla.nwt.react;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Waldemar L on 05.05.2017.
 */

class DBHelper extends SQLiteOpenHelper {

    static final String DB_NAME = "result.db";
    static final int DB_VERSION = 1;


    // Columns
    static final String DB_TABLE_NAME = "result";
    static final String DB_COL_TIMESTAMP = "timestamp";
    static final String DB_COL_USER = "user";
    static final String DB_COL_ID = "rowid";
    static final String DB_COL_TIME_TO_START = "timetostart";
    static final String DB_COL_TIME_TO_REACT = "timetoreact";
    static final String DB_COL_TIME_MIN = "timemin";
    static final String DB_COL_TIME_MAX = "timemax";
    static final String DB_COL_GENDER = "gender";


    DBHelper(Context context){

        super(context,DB_NAME,null,DB_VERSION);
    }

    SQLiteDatabase Database;
    Cursor cursor = null;
    @Override
    public void onCreate(SQLiteDatabase db) {
        String makeTable = "CREATE TABLE "+ DB_TABLE_NAME +"("+DB_COL_ID+" INTEGER INCREMENT,"+DB_COL_TIMESTAMP+" TEXT,"+DB_COL_USER+" TEXT,"+DB_COL_GENDER+" TEXT,"+DB_COL_TIME_MIN+" INTEGER,"+DB_COL_TIME_MAX+" INTEGER,"+DB_COL_TIME_TO_START+" INTEGER,"+DB_COL_TIME_TO_REACT+" INTEGER)";
        db.execSQL(makeTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" +DB_TABLE_NAME);
        onCreate(db);
        Database = db;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);Database = db;
    }
    public void addEntry(String username,boolean gender,int time_min,int time_max,int time_to_init,int time_to_react){
        String _gender;
        if(gender){
            _gender = "M";
        }
        else{
            _gender = "W";
        }
        username = username.toUpperCase();
        String timestamp  = new SimpleDateFormat("dd.MM.yyyy' at 'HH:mm:ss").format(new Date());
        // add Entry
        Database.execSQL("INSERT INTO "+DB_TABLE_NAME+" ("+DB_COL_TIMESTAMP+","+DB_COL_USER+","+DB_COL_GENDER+","+DB_COL_TIME_MIN+","+DB_COL_TIME_MAX+","+DB_COL_TIME_TO_START+","+DB_COL_TIME_TO_REACT+") VALUES ("+timestamp+","+username+","+_gender+","+time_min+","+time_max+","+time_to_init+","+time_to_react+");");
        Database.close();

    }
    public void addEntry_w_Entry(Entry entry){
        boolean gender = entry.getGender();
        String username = entry.getName();
        String timestamp = entry.getTimestamp();
        int time_min = entry.getT_min();
        int time_max = entry.getT_maxm();
        int time_to_init = entry.getT_to_init();
        int time_to_react = entry.getT_to_react();
        String _gender;
        if(gender){
            _gender = "M";
        }
        else{
            _gender = "W";
        }
        username = username.toUpperCase();

        // add Entry
        Database.execSQL("INSERT INTO "+DB_TABLE_NAME+" ("+DB_COL_TIMESTAMP+","+DB_COL_USER+","+DB_COL_GENDER+","+DB_COL_TIME_MIN+","+DB_COL_TIME_MAX+","+DB_COL_TIME_TO_START+","+DB_COL_TIME_TO_REACT+") VALUES ("+timestamp+","+username+","+_gender+","+time_min+","+time_max+","+time_to_init+","+time_to_react+");");
        Database.close();
    }
    public void deleteDB(){
        Database.execSQL("DELETE * FROM "+DB_TABLE_NAME);
        Database.close();
    }
    public Entry getEntry(int row_id){
        Entry entry = new Entry();
            cursor = Database.rawQuery("SELECT * FROM "+DB_TABLE_NAME+" WHERE "+DB_COL_ID+" = ?", new String[]{row_id+""});
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                entry.setTimestamp(cursor.getString(cursor.getColumnIndexOrThrow(DB_COL_TIMESTAMP)));
                entry.setName(cursor.getString(cursor.getColumnIndexOrThrow(DB_COL_USER)));
                String _gender = cursor.getString(cursor.getColumnIndexOrThrow(DB_COL_GENDER));
                if(_gender.equals("M") ) entry.setGender(true);
                else entry.setGender(false);
                entry.setT_min(cursor.getInt(cursor.getColumnIndexOrThrow(DB_COL_TIME_MIN)));
                entry.setT_maxm(cursor.getInt(cursor.getColumnIndexOrThrow(DB_COL_TIME_MAX)));
                entry.setT_to_init(cursor.getInt(cursor.getColumnIndexOrThrow(DB_COL_TIME_TO_START)));
                entry.setT_to_react(cursor.getInt(cursor.getColumnIndexOrThrow(DB_COL_TIME_TO_REACT)));


            }
            else entry = null;
        return entry;
    }
    public List<Entry> getAllEntries(){
        List<Entry> list = new ArrayList<>();
        return list;
    }



}
