package dwla.nwt.react;


import android.app.Activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Observable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class versuch extends Fragment {
    int primary, secondary, alt;
    TextView versuch_center_msg,versuch_header;
    Button btn_help;
    LinearLayout versuch_background;






    public versuch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_versuch, container, false);
        versuch_center_msg = (TextView)v.findViewById(R.id.versuch_centermsg);
        versuch_header = (TextView)v.findViewById(R.id.versuch_header);
        versuch_background = (LinearLayout)v.findViewById(R.id.experiment_background);
        btn_help = (Button)v.findViewById(R.id.button_versuch_help);
        versuch_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(((App) getActivity().getApplication()).getTimeMin() == -1 || ((App) getActivity().getApplication()).getTimeMax() == -1) Toast.makeText(getContext(),"Bitte in den Einstellungen alle Werte angeben.", Toast.LENGTH_LONG).show();
              else if(((App) getActivity().getApplication()).getTimeMin() > ((App) getActivity().getApplication()).getTimeMax()) Toast.makeText(getContext(),"Minimalzeit muss kleiner als Maximalzeit sein.", Toast.LENGTH_LONG).show();
              else
                {
                Intent startExperiment = new Intent(getActivity().getBaseContext(), experiment.class);
                startActivity(startExperiment);
              }

            }
        });
        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog.Builder versuch_Dialog = new MaterialDialog.Builder(getContext());
                versuch_Dialog.content("Tippen Sie auf den Bildschirm bis die Anweisung komt zu warten, bis der Bildschirm farbig wird. Nun muss möglichst schnell auf den Bildschirm gedrückt werden. Falls zu früh reagiert wurde wird der Versuch nicht gezählt.").show();
            }
        });
        if (getActivity() instanceof Main) {
        ((Main) getActivity()).setVersuchFragment(this);
        }
        setUI();

        return v;
    }
    void setUI() {



        if (((App) getActivity().getApplication()).isGender_isMale()) {
            primary = getResources().getColor(R.color.palette_blue_mid);
            secondary = getResources().getColor(R.color.palette_blue_dark);
            alt = getResources().getColor(R.color.palette_blue_alt);
        } else {
            primary = getResources().getColor(R.color.palette_orange_mid);
            secondary = getResources().getColor(R.color.palette_orange_dark);
            alt = getResources().getColor(R.color.palette_orange_alt);

        }
        versuch_header.setTextColor(primary);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getActivity().getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.setStatusBarColor(primary);


        }
        Activity parent = getActivity();
        if (parent instanceof Main) {
            ((Main) parent).changeTabColor(primary, alt);
        }

    }






}


