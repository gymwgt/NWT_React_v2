package dwla.nwt.react;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.transition.Visibility;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class info extends Fragment {

    //VARS
    View v;
    TextView info1t,info1h,    info2t,info2h,     info3t,info3h,     info4t,info4h,     info5t,info5h;
    LinearLayout info1l,       info2l,            info3l,            info4l,            info5l;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         v = inflater.inflate(R.layout.fragment_info,container,false);
        info1h = (TextView)v.findViewById(R.id.info_obj1_header);
        info2h = (TextView)v.findViewById(R.id.info_obj2_header);
        info3h = (TextView)v.findViewById(R.id.info_obj3_header);
        info4h = (TextView)v.findViewById(R.id.info_obj4_header);
        info5h = (TextView)v.findViewById(R.id.info_obj5_header);

        info1t = (TextView)v.findViewById(R.id.info_obj1_text);
        info2t = (TextView)v.findViewById(R.id.info_obj2_text);
        info3t = (TextView)v.findViewById(R.id.info_obj3_text);
        info4t = (TextView)v.findViewById(R.id.info_obj4_text);
        info5t = (TextView)v.findViewById(R.id.info_obj5_text);

        info1l = (LinearLayout)v.findViewById(R.id.info_obj1);
        info2l = (LinearLayout)v.findViewById(R.id.info_obj2);
        info3l = (LinearLayout)v.findViewById(R.id.info_obj3);
        info4l = (LinearLayout)v.findViewById(R.id.info_obj4);
        info5l = (LinearLayout)v.findViewById(R.id.info_obj5);
        createOnClickListeners();
        setUI();
        return v;
    }

    void createOnClickListeners(){
        info1l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(info1t.getVisibility() == View.VISIBLE){info1t.setVisibility(View.GONE);}
                else {  info1t.setVisibility(View.VISIBLE);
                        info2t.setVisibility(View.GONE);
                        info3t.setVisibility(View.GONE);
                        info4t.setVisibility(View.GONE);
                        info5t.setVisibility(View.GONE);}
            }
        });
        info2l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(info2t.getVisibility() == View.VISIBLE){info2t.setVisibility(View.GONE);}
                else {  info2t.setVisibility(View.VISIBLE);
                        info1t.setVisibility(View.GONE);
                        info3t.setVisibility(View.GONE);
                        info4t.setVisibility(View.GONE);
                        info5t.setVisibility(View.GONE);}
            }
        });
        info3l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(info3t.getVisibility() == View.VISIBLE){info3t.setVisibility(View.GONE);}
                else {  info3t.setVisibility(View.VISIBLE);
                        info1t.setVisibility(View.GONE);
                        info2t.setVisibility(View.GONE);
                        info4t.setVisibility(View.GONE);
                        info5t.setVisibility(View.GONE);}
            }
        });
        info4l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(info4t.getVisibility() == View.VISIBLE){info4t.setVisibility(View.GONE);}
                else {  info4t.setVisibility(View.VISIBLE);
                        info1t.setVisibility(View.GONE);
                        info2t.setVisibility(View.GONE);
                        info3t.setVisibility(View.GONE);
                        info5t.setVisibility(View.GONE);}
            }
        });
        info5l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(info5t.getVisibility() == View.VISIBLE){info5t.setVisibility(View.GONE);}
                else {  info5t.setVisibility(View.VISIBLE);
                        info1t.setVisibility(View.GONE);
                        info2t.setVisibility(View.GONE);
                        info3t.setVisibility(View.GONE);
                        info4t.setVisibility(View.GONE);
                }
            }
        });








    }
    void setUI() {
        int primary;
        int alt;
        if (((App) getActivity().getApplication()).isGender_isMale()) {
            primary = getResources().getColor(R.color.palette_blue_mid);
            alt= getResources().getColor(R.color.palette_blue_alt);
        }
        else {
            primary = getResources().getColor(R.color.palette_orange_mid);
            alt= getResources().getColor(R.color.palette_orange_alt);
        }
        info1h.setTextColor(primary);
        info2h.setTextColor(primary);
        info3h.setTextColor(primary);
        info4h.setTextColor(primary);
        info5h.setTextColor(primary);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getActivity().getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.setStatusBarColor(primary);

        }
        Activity parent = getActivity();
        if(parent instanceof Main){
            ((Main)parent).changeTabColor(primary,alt);
        }

    }
}

