package dwla.nwt.react;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static dwla.nwt.react.App.Entrylist;


public class speichern extends Fragment {

    Button btn_speichern,btn_löschen,btn_teilen,btn_speichern_info;
    TextView header;
    public speichern() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_speichern, container, false);
        btn_speichern = (Button)v.findViewById(R.id.btn_speichern);
        btn_löschen = (Button)v.findViewById(R.id.btn_loschen);
        btn_teilen = (Button)v.findViewById(R.id.btn_teilen);
        btn_speichern_info = (Button)v.findViewById(R.id.btn_speichern_info);
        header = (TextView)v.findViewById(R.id.speichern_header);

        setUI();
        return v;
    }



    void setUI() {
        int primary,secondary,alt;
        Drawable btn_color,btn_color2,btn_color3;
        if (((App) getActivity().getApplication()).isGender_isMale()) {
            primary = getResources().getColor(R.color.palette_blue_mid);
          //  secondary = getResources().getColor(R.color.palette_blue_dark);
            alt = getResources().getColor(R.color.palette_blue_alt);
            btn_color = ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.button_blue);
            btn_color2 = ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.button_blue2);
            btn_color3 = ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.button_blue3);
        }
        else {
            primary = getResources().getColor(R.color.palette_orange_mid);
        //    secondary = getResources().getColor(R.color.palette_orange_dark);
            alt = getResources().getColor(R.color.palette_orange_alt);
            btn_color = ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.button_orange);
            btn_color2 = ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.button_orange2);
            btn_color3 = ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.button_orange3);

        }
        btn_speichern.setBackground(btn_color);
        btn_teilen.setBackground(btn_color3);

        header.setTextColor(primary);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getActivity().getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.setStatusBarColor(primary);

        }
        Activity parent = getActivity();
        if(parent instanceof Main){
            ((Main)parent).changeTabColor(primary,alt);
        }
        btn_speichern_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog.Builder speichern_saveDialog = new MaterialDialog.Builder(getContext());
                speichern_saveDialog.content("Damit die Werte nicht verloren werden müssen diese gespeichert werden. Dabei wird eine Excel-Datei auf die SD Karte geschrieben. Alternativ kann man auch den Datensatz per bspw. Whatsapp oder EMail teilen. Um eine neue Versuchsreihe zu starten sollten die Werte davor durch das anklicken des Häckchens im Speichern-Popup bzw. durch die Verwendung von \" Löschen \" gelöscht werden. ").show();
            }
        });
        btn_speichern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((App) getActivity().getApplication()).Entrylist.isEmpty()){Toast.makeText(getActivity().getApplicationContext(),"Es sind keine Einträge vorhanden.", Toast.LENGTH_SHORT).show();}
                else {
                    final MaterialDialog.Builder saveDialog = new MaterialDialog.Builder(getContext());

                    saveDialog.title("Speichern")
                            .content("Mit welchem Namen soll abgespeichert werden?")
                            .inputType(InputType.TYPE_CLASS_TEXT)
                            .input("Nur alphanumerische Zeichen erlaubt.", "", new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(MaterialDialog dialog, CharSequence input) {
                                    input = input.toString().replace(" ", "");
                                    if (StringUtils.isAlphanumeric(input) || input.length() == 0)
                                        abspeichern(input);
                                    else
                                        Toast.makeText(getActivity().getApplicationContext(), "Bitte nur alphanumerische Zeichen verwenden. Alternativ kann das Feld frei gelassen werden.", Toast.LENGTH_SHORT).show();
                                    if(dialog.isPromptCheckBoxChecked()){ deleteEntries() ;}
                                }

                            })
                            .checkBoxPrompt("Einträge nach Speichern löschen?", false, null)
                            .positiveText("Speichern")
                            .negativeText("Abbrechen")
                            .show();
                }
            }});

        btn_löschen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((App) getActivity().getApplication()).Entrylist.isEmpty()){
                    Toast.makeText(getActivity().getApplicationContext(), "Es gibt nichts zu löschen.", Toast.LENGTH_SHORT).show();
                }
                else{
                    final MaterialDialog.Builder deleteDialog = new MaterialDialog.Builder(getContext());
                    deleteDialog.title("Einträge löschen")
                            .content("Möchten sie alle "+((App) getActivity().getApplication()).Entrylist.size()+" Einträge löschen? \nDavon sind nur nicht abgespeicherte Einträge betroffen")
                            .positiveText("Löschen")
                            .negativeText("Abbrechen")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    deleteEntries();
                                    Toast.makeText(getContext().getApplicationContext(),"Erfolgreich alle Einträge gelöscht.", Toast.LENGTH_SHORT).show();

                                }
                            })
                            .show();
                }
            }
        });
        btn_teilen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Entry> entries = ((App) getActivity().getApplication()).Entrylist;
                if(((App) getActivity().getApplication()).Entrylist.isEmpty())  Toast.makeText(getActivity().getApplicationContext(), "Es gibt nichts zu löschen.", Toast.LENGTH_SHORT).show();
                else{
                    String stream = "";
                    for (Entry entry: entries)
                    {
                        String gender;
                        // TIMESTAMP | NAME | GENDER | T_MIN | T_MAX | T_TO_INIT | T_TO_REACT \n
                        if(entry.getGender()){gender="m";}else {gender = "w";}
                        stream = stream + entry.getTimestamp()+","+entry.getName()+","+gender+","+entry.getT_min()+","+entry.getT_maxm()+","+entry.getT_to_init()+","+entry.getT_to_react()+"\n";
                    }
                    Intent shareData = new Intent();
                    File sdcard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdcard.getAbsolutePath()+"/react/temp");
                    boolean doesDirExist = dir.exists();
                    if(!doesDirExist)  {  doesDirExist = dir.mkdirs(); }    // IF SDCARDPATH./REACT/ !EXISTS → CREATE PATH
                    if(!doesDirExist) Toast.makeText(getActivity().getApplicationContext(),"FEHLER: SDCARD\\REACT\\* KONNTE NICHT ERSTELLT WERDEN! SPEICHERN NICHT MÖGLICH",Toast.LENGTH_LONG).show();
                    File file = new File(dir,"temp.csv");
                    try {
                        FileOutputStream stream_share = new FileOutputStream(file);
                        stream_share.write(stream.getBytes());
                        stream_share.flush();
                        stream_share.close();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    Uri F = Uri.fromFile(file);
                    shareData.setAction(Intent.ACTION_SEND)
                             .putExtra(Intent.EXTRA_STREAM,F)
                    .setType("*/*");
                    //   .setType("applications/csv");
                    startActivity(Intent.createChooser(shareData,"Datensatz teilen."));


                }
            }
        });
        }



        void abspeichern(CharSequence i){
            String filename;
            String datastream = "";
             List<Entry> entries = ((App) getActivity().getApplication()).Entrylist;
            if(i.length() == 0){
                filename =  new SimpleDateFormat("dd.MM.yyyy'@'HH:mm:ss").format(new Date())+".csv";
            }
            else{
                filename = i+".csv";
            }
            for (Entry entry: entries)
            {
                String gender;
                // TIMESTAMP | NAME | GENDER | T_MIN | T_MAX | T_TO_INIT | T_TO_REACT \n
                if(entry.getGender()){gender="m";}else {gender = "w";}
                datastream = datastream + entry.getTimestamp()+","+entry.getName()+","+gender+","+entry.getT_min()+","+entry.getT_maxm()+","+entry.getT_to_init()+","+entry.getT_to_react()+"\n";
            }
            File sdcard = Environment.getExternalStorageDirectory();
            File dir = new File(sdcard.getAbsolutePath()+"/react");
            boolean doesDirExist = dir.exists();
            if(!doesDirExist)  {  doesDirExist = dir.mkdirs(); }    // IF SDCARDPATH./REACT/ !EXISTS → CREATE PATH
            if(!doesDirExist) Toast.makeText(getActivity().getApplicationContext(),"FEHLER: SDCARD\\REACT\\* KONNTE NICHT ERSTELLT WERDEN! SPEICHERN NICHT MÖGLICH",Toast.LENGTH_LONG).show();
            File file = new File(dir,filename);
            try {
                FileOutputStream stream = new FileOutputStream(file);
                stream.write(datastream.getBytes());
                stream.flush();
                stream.close();
                Toast.makeText(getContext().getApplicationContext(),"Erfolgreich abgespeichert: "+dir.toString(), Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();

            }

        }
        void deleteEntries(){
           try {
               Thread.sleep(100);

               if (((App) getActivity().getApplication()).Entrylist.isEmpty()) {
                   Toast.makeText(getContext().getApplicationContext(), "Nicht gelöscht da keine Einträge vorhanden sind.", Toast.LENGTH_SHORT).show();
               } else {
                   ((App) getActivity().getApplication()).Entrylist = new ArrayList<>();
                   Toast.makeText(getContext().getApplicationContext(), "Erfolgreich alle Einträge gelöscht.", Toast.LENGTH_SHORT).show();

               }
           }
           catch (Exception e){
                Toast.makeText(getContext().getApplicationContext(), "FEHLER: Löschen nicht erfolgreich: "+e, Toast.LENGTH_SHORT).show();
           }


        }
}
