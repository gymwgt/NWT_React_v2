package dwla.nwt.react;

import android.content.ContentValues;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Handler;
import java.io.Writer;
import java.util.ArrayList;

import org.w3c.dom.Text;

public class experiment extends AppCompatActivity {
    Switch ioswitch;TextView ioswitch_txt;
    TextView backgrd_timeindicator;
    LinearLayout backgrd ;
    int timemin, timemax, randomdelay;
    int primary,secondary,white;
    long TimeAtStart, TimeItTook;
    int counterExperiments = 0;
    boolean isTestRunning = false,didTestFinish = true,WasLastCheatAttempt = false, didTestinitiate = false;
    Button btn_counter;
    static List<String> ergebnis = new ArrayList<>(); // TIMESTAMP | TIME_MIN | TIME_MAX | TIME_TO_INITIATE | TIME_TO_REACT



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiment);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        backgrd= (LinearLayout)findViewById(R.id.background_experiment);
        setColor();
        timemin =  ((App) getApplication()).getTimeMin();
        timemax =   ((App) getApplication()).getTimeMax();

        btn_counter =  (Button)findViewById(R.id.btn_exp_counter);
        backgrd_timeindicator = (TextView)findViewById(R.id.reflex_time);

        backgrd_timeindicator.setTextColor(primary);
        btn_counter.setTextColor(primary);
        btn_counter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog.Builder d = new MaterialDialog.Builder(experiment.this);
                d.title(counterExperiments+ " Versuche");
                d.neutralText("Zurücksetzen");
                d.onNeutral(new MaterialDialog.SingleButtonCallback(){
                    @Override
                    public void onClick(@NonNull MaterialDialog d, @NonNull DialogAction w){
                        counterExperiments = 0;
                        btn_counter.setText(getResources().getString(R.string.versuch_btn_anz)+" "+counterExperiments);
                    }
                });
                d.show();
            }
        });

        backgrd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent m) {
            // if Test is Running
            if(m.getAction() == MotionEvent.ACTION_DOWN){
                if(WasLastCheatAttempt){
                    backgrd_timeindicator.setText(R.string.exp_presstorestart);
                    backgrd.setBackgroundColor(white);
                    WasLastCheatAttempt = false;
                    didTestFinish = true;
                    isTestRunning = false;
                    didTestinitiate=false;
                    btn_counter.setVisibility(View.VISIBLE);
                    backgrd_timeindicator.setTextColor(primary);
                    btn_counter.setTextColor(primary);

                }





                else if(isTestRunning && !didTestFinish){
                    isTestRunning = false;
                    didTestFinish = true;
                    TimeItTook = System.currentTimeMillis()-TimeAtStart;
                    backgrd.setBackgroundColor(white);
                    backgrd_timeindicator.setTextColor(primary);
                    btn_counter.setVisibility(View.VISIBLE);
                    //addValsToDB();
                    addtoList();

                }
                else if(!isTestRunning && didTestFinish && didTestinitiate){
                    // Prevent cheating
                    isTestRunning = false;
                    backgrd.setBackgroundColor(getResources().getColor(R.color.palette_alt_warningred));
                    backgrd_timeindicator.setTextColor(white);
                    btn_counter.setTextColor(white);
                    WasLastCheatAttempt = true;
                    backgrd_timeindicator.setText(R.string.exp_cheat);
                    didTestinitiate = false;
                    btn_counter.setVisibility(View.VISIBLE);


                }
                else if(!isTestRunning && didTestFinish && !didTestinitiate){
                    backgrd_timeindicator.setText(R.string.exp_waiting);
                    btn_counter.setVisibility(View.INVISIBLE);
                    runTest();


                }
            // if Test got stopped already - to reinstate Test.

                }
                return true;
            }
        });
    }
    protected void onResume(Bundle savedInstance){
        super.onResume();
        finish();
    }
    public void addtoList(){
        counterExperiments++;
        btn_counter.setText(getResources().getString(R.string.versuch_btn_anz)+" "+counterExperiments);
        String timestamp;
        timestamp = new SimpleDateFormat("dd.MM.yyyy 'um' HH:mm:ss").format(new Date());
        Entry entry = new Entry();
        entry.setT_min(timemin);
        entry.setT_maxm(timemax);
        entry.setName( ((App) getApplication()).getName());
        entry.setGender(((App) getApplication()).isGender_isMale());
        entry.setT_to_react((int)TimeItTook);
        entry.setT_to_init(randomdelay);
        entry.setTimestamp(timestamp);
        ((App) getApplication()).addEntry(entry);
    }

    public void setColor(){
        white = getApplication().getResources().getColor(R.color.palette_greyscale_purewhite);
        if(((App) getApplication()).gender_isMale){
            primary = getApplication().getResources().getColor(R.color.palette_blue_mid);
            secondary = getApplication().getResources().getColor(R.color.palette_blue_dark);

        }
        else{
            primary = getApplication().getResources().getColor(R.color.palette_orange_mid);
            secondary = getApplication().getResources().getColor(R.color.palette_orange_dark);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.setStatusBarColor(primary);

        }
    }

    public void runTest(){
            didTestinitiate = true;

            randomdelay = (int)((Math.random()*(timemax-timemin))+timemin);


                // Timer for Delay from Experiment Start
               Timer t = new Timer();
                t.schedule(new TimerTask() {
                   @Override
                   public void run() {

                       TimeAtStart = System.currentTimeMillis();
                       isTestRunning = true;
                       didTestFinish = false;
                       runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                              if(didTestinitiate) {backgrd.setBackgroundColor(primary); backgrd_timeindicator.setTextColor(white); }
                               else{
                                  isTestRunning = false;
                                  didTestFinish = true;
                              }
                           }
                       });

                        // Timer for Update of Background Millisecs Indicator
                       Timer s = new Timer();
                       s.scheduleAtFixedRate(new TimerTask() {
                           @Override
                           public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(isTestRunning)backgrd_timeindicator.setText(((int) (System.currentTimeMillis() - TimeAtStart)) + " ms");

                                if(!isTestRunning && !WasLastCheatAttempt){
                                    backgrd_timeindicator.setText(((int) (System.currentTimeMillis() - TimeAtStart)) + " ms \n \n"+getResources().getString(R.string.exp_presstorestart));
                                    didTestinitiate = false;
                                }
                                }
                            });
                            if(!isTestRunning) {
                                this.cancel();
                            }
                           }
                       },0,40);
                   }
               },randomdelay);





    }


}
