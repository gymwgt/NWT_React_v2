package dwla.nwt.react;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class App extends Application {
   // GLOBAL VARS ARE STORED HERE!
    int timeMin = -1,timeMax = -1;
    String name = "";
    boolean gender_isMale = true;
    static List<Entry> Entrylist = new ArrayList<>();



    @Override
    public void onCreate(){
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("arvo.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    public void addEntry(Entry entry){
        Entrylist.add(entry);
    }
    public void resetList(){
        Entrylist.clear();
    }

    public String getName() {
        return name;
    }

    public int getTimeMax() {
        return timeMax;
    }

    public int getTimeMin() {
        return timeMin;
    }

    public boolean isGender_isMale() {
        return gender_isMale;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setGender_isMale(boolean gender_isMale) {
        this.gender_isMale = gender_isMale;
    }

    public void setTimeMax(int timeMax) {
        this.timeMax = timeMax;
    }

    public void setTimeMin(int timeMin) {
        this.timeMin = timeMin;
    }
}
