package dwla.nwt.react;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dwla.nwt.react.R;
import dwla.nwt.react.auswertung;
import dwla.nwt.react.einstellungen;
import dwla.nwt.react.info;
import dwla.nwt.react.speichern;
import dwla.nwt.react.versuch;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Main extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Fragment m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

       // toolbar = (Toolbar)findViewById(R.id.toolbar);
       // getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);//
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setIcons();

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new einstellungen(), "\uF013");
        viewPagerAdapter.addFragment(new versuch(), "\uF0C3");
        viewPagerAdapter.addFragment(new speichern(), "\uF0C7");
        viewPagerAdapter.addFragment(new info(), "\uF05A");
        viewPager.setAdapter(viewPagerAdapter);
    }
    private void setIcons() {

        Typeface font = Typeface.createFromAsset(getAssets(),"fontawesome-webfont.ttf");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font);
                }
            }
        }
    }
    void changeTabColor(int i,int v){
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setBackgroundColor(i);
        tabLayout.setSelectedTabIndicatorColor(v);

    }
    void setVersuchFragment(Fragment fragment){
        m = fragment;
    }
    Fragment getVersuchFragment(){
        return m;
    }
    void refreshVersuchFragment(){
       try{  FragmentTransaction fragmentTransaction = (getSupportFragmentManager().beginTransaction());
        fragmentTransaction.detach(m).attach(m).commit();}
       catch (Exception e){}
    }


}


class ViewPagerAdapter extends FragmentPagerAdapter{
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager){
        super(manager);
    }
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }


}
