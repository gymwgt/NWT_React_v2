package dwla.nwt.react;

/**
 * Created by Waldemar L on 22.06.2017.
 */
class Entry extends Object{
    private int t_min, t_max ,t_to_init,t_to_react;
    private String timestamp,name;
    boolean gender;

    public int getT_maxm() {
        return t_max;
    }

    public int getT_min() {
        return t_min;
    }

    public int getT_to_init() {
        return t_to_init;
    }

    public int getT_to_react() {
        return t_to_react;
    }

    public boolean getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setT_maxm(int t_max) {
        this.t_max = t_max;
    }

    public void setT_min(int t_min) {
        this.t_min = t_min;
    }

    public void setT_to_init(int t_to_init) {
        this.t_to_init = t_to_init;
    }

    public void setT_to_react(int t_to_react) {
        this.t_to_react = t_to_react;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
