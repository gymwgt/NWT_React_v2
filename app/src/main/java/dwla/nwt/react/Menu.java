package dwla.nwt.react;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Method;


public class Menu extends AppCompatActivity {
    // All elements w/ logic
    TextView fa_settings, fa_experiment, fa_result, fa_about;
    EditText timemin, timemax;
    LinearLayout  div_experiment, div_result, div_about,div_settings;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // ButterKnife.bind(this);
        setContentView(R.layout.activity_menu);

            fa_settings = (TextView)findViewById(R.id.fa_settings);             fa_experiment = (TextView)findViewById(R.id.fa_experiment);     fa_result = (TextView)findViewById(R.id.fa_result);                     fa_about = (TextView)findViewById(R.id.fa_about);
            div_settings= (LinearLayout)findViewById(R.id.menu_entry_settings); div_about= (LinearLayout)findViewById(R.id.menu_entry_about);   div_experiment= (LinearLayout)findViewById(R.id.menu_entry_experiment); div_result= (LinearLayout)findViewById(R.id.menu_entry_result);
            timemin=(EditText)findViewById(R.id.time_min);                      timemax=(EditText)findViewById(R.id.time_max);
        initializeFA();
    }

    // Event Listeners
    public void settings_open_close(View v){
    LinearLayout hiddenSettings = (LinearLayout)findViewById(R.id.hiddenSettings);
        if(hiddenSettings.getVisibility() == View.GONE){
            hiddenSettings.setVisibility(View.VISIBLE);
        }
        else{
            hiddenSettings.setVisibility(View.GONE);
        }
    }

    public void openExperiments(View v){
        if(TextUtils.isEmpty(timemax.getText()) && TextUtils.isEmpty(timemin.getText())){
            // IF TIME_MAX >AND< TIME_MIN IS NOT SET
           alert("Fehler", "Bitte zuerst Versuch in den Einstellungen konfigurieren", true, "OK.",this);
        }
        else if(TextUtils.isEmpty(timemax.getText())){
            alert("Fehler","Maximale Dauer muss in den Einstellungen konfiguriert werden",true,"OK.",this);
        }
        else if(TextUtils.isEmpty(timemin.getText())){
            alert("Fehler","Minimale Dauer muss in den Einstellugen konfiguriert werden",true,"OK.",this);
        }
        else if (ismaxval_bigger_minval()){
            alert("Fehler","Minimale Dauer muss kleiner als die Maximale Dauer sein",true,"OK.",this);
        }

        else{
            // OPEN EXPERIMENT METHOD
            Intent startExperiment = new Intent(getBaseContext(), experiment.class);
            // PASS VARS
            startExperiment.putExtra("TIME_MIN",Integer.parseInt(timemin.getText().toString()));
            startExperiment.putExtra("TIME_MAX",Integer.parseInt(timemax.getText().toString()));
            startActivity(startExperiment);
            //startActivity(new Intent(this, experiment.class));
        }
    }

    public boolean ismaxval_bigger_minval(){
        if(Integer.parseInt(timemin.getText().toString()) > Integer.parseInt(timemax.getText().toString())) {
        return  true;
        }
        else{
        return false;
        }
    }
    public void initializeFA(){
        Typeface FA = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        fa_settings.setTypeface(FA);
        fa_about.setTypeface(FA);
        fa_experiment.setTypeface(FA);
        fa_result.setTypeface(FA);
    }

    static void alert(String title, String message, boolean hasPOSbtn,  String POSbtnText, Context context){
      try {
          AlertDialog.Builder d = new AlertDialog.Builder(context);
          d.setTitle(title).setMessage(message).setIcon(android.R.drawable.ic_dialog_info);
          if (hasPOSbtn) {
              d.setPositiveButton(POSbtnText, new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface i, int which) {

                  }
              });
          d.show();
          }
      }
      catch (Exception e){}
    }

}



